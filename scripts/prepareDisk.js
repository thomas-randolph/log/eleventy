var mkdirp =  require( "mkdirp" );

var cwd = process.cwd();

mkdirp.sync( `${cwd}/generate` );
mkdirp.sync( `${cwd}/src/data` );
mkdirp.sync( `${cwd}/public/css` );
