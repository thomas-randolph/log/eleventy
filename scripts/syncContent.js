/* eslint-disable no-process-env */

var { writeFileSync } = require( "fs" );
var { normalize } = require( "path" );

var { makeConnection, getEntries } = require( "./common/Contentful.js" );

var reader = makeConnection();

( async () => {
	let authorsContent = await getEntries( reader, "author" );
	let longPostsContent = await getEntries( reader, {
		"content_type": "long",
		"order": "-fields.authored",
		"include": 10
	} );
	let shortPostsContent = await getEntries( reader, {
		"content_type": "short",
		"order": "-fields.authored",
		"include": 10
	} );
	let authors = reader.parseEntries( authorsContent ).items;
	let longPosts = reader.parseEntries( longPostsContent ).items;
	let [ textShortPosts, imageShortPosts ] = reader
		.parseEntries( shortPostsContent )
		.items
		.reduce( ( allShortPosts, post ) => {
			if( post.fields.images ){
				allShortPosts[ 1 ].push( post );
			}
			else{
				allShortPosts[ 0 ].push( post );
			}

			return allShortPosts;
		}, [ [], [] ] );

	writeFileSync( normalize( `${__dirname}/../generate/authors.json` ), JSON.stringify( authors, null, 4 ), "utf8" );
	writeFileSync( normalize( `${__dirname}/../generate/longPosts.json` ), JSON.stringify( longPosts, null, 4 ), "utf8" );
	writeFileSync( normalize( `${__dirname}/../generate/shortPosts.json` ), JSON.stringify( textShortPosts, null, 4 ), "utf8" );
	writeFileSync( normalize( `${__dirname}/../generate/imageShortPosts.json` ), JSON.stringify( imageShortPosts, null, 4 ), "utf8" );
} )();
