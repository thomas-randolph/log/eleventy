/* eslint-disable no-process-env */
var Contentful = require( "contentful" );

const SPACE = process.env.CONTENTFUL_SPACE || "98dlsspfha2e";
const PREVIEW_TOKEN = process.env.CONTENTFUL_PREVIEW_TOKEN;

var baseSettings = {
	"host": "preview.contentful.com",
	"space": SPACE,
	"accessToken": PREVIEW_TOKEN
};

function makeConnection(){
	var settings = Object.assign( {}, baseSettings );

	return Contentful.createClient( settings );
}

async function getEntries( conn, options ){
	if( !conn ){
		conn = makeConnection();
	}

	if( typeof options == "string" ){
		options = {
			"content_type": options
		};
	}

	return await conn.getEntries( options );
}

module.exports = {
	SPACE,
	PREVIEW_TOKEN,
	makeConnection,
	getEntries
};
