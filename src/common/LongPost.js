var { id, isDraft } = require( "./Post.js" );

function slug( post ){
	var title = post.fields.title;

	return title.replace( /(?:[^A-Z0-9])+/gi, "-" ).toLowerCase();
}

function permalink( post ){
	return `/post/${slug( post )}.html`;
}

module.exports = { id, isDraft, slug, permalink };
