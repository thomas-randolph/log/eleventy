import { css, unsafeCSS } from "./Component.js";

export function base(){
	return css`
		:host{
			box-sizing: border-box;
			display: block;
		}

		:host *{
			box-sizing: inherit;
		}

		/*****************************
		 * TAGS - BUTTON
		 *****************************/

		button{
			background-color: var( --color-info-subtle );
			border: 1px solid var( --color-info );
			border-radius: var( --size-radius );
			padding: 0.75rem 1.25rem;
		}

		button.alert{
			background-color: var( --color-alert-subtle );
			border-color: var( --color-alert );
		}

		button.inline{
			padding: 0.1rem 0.5rem
		}

		button.disabled,
		button:disabled{
			background-color: var( --color-neutral-subtle );
			border-color: var( --color-neutral );
			color: var( --color-text-faded );
		}

		/*****************************
		 * TAGS - LABEL
		 *****************************/

		:not(fieldset) > label:first-of-type{
			font-size: var( --size-small-text );
		}

		/*****************************
		 * TAGS - FIELDSET
		 *****************************/

		fieldset.pills{
			display: flex;
			overflow: hidden;
			white-space: nowrap;
		}

		/*****************************
		 * TAGS - LEGEND
		 *****************************/

		fieldset legend:first-of-type{
			font-size: var( --size-small-text );
		}

		/*****************************
		 * TAGS - INPUT
		 *****************************/

		input{
			background-color: var( --color-brightest );
			border: 1px solid var( --color-strut-input );
			border-radius: var( --size-radius );
			margin: 0;
			padding: 0.75rem 1rem;
			vertical-align: sub;
		}
		input[type="file"]{
			padding: 0.5rem 1rem;
		}
		input[type="radio"]{
			vertical-align: initial;
		}

		input:focus,
		input:active{
			border-bottom-width: 2px;
			border-color: var( --color-strut-focus );
			margin-bottom: -1px;
		}

		input:disabled{
			background-color: var( --color-neutral );
		}

		fieldset.pills input{
			border: 0;
			clip: rect(0, 0, 0, 0);
			height: 1px;
			overflow: hidden;
			position: absolute;
			width: 1px;
		}

		/*****************************
		 * TAGS - LABEL
		 *****************************/

		fieldset.pills label{
			background-color: var( --color-neutral-subtle );
			border: 1px solid var( --color-strut-input );
			line-height: 1;
			margin-right: -1px;
			padding: 0.75rem 1.5rem;
			text-align: center;
			transition: var( --vfx-global-transition );
		}

		fieldset.pills label:hover{
			cursor: pointer;
		}

		fieldset.pills input:checked + label{
			background-color: var( --color-okay-subtle );
			border-color: var( --color-okay );
			box-shadow:
				inset var( --vfx-right-shadow-block-default ),
				inset var( --vfx-left-shadow-block-default );
		}
		fieldset.pills input:checked + label:last-of-type{
			box-shadow: inset var( --vfx-left-shadow-block-default );
		}
		fieldset.pills input:checked + label:first-of-type{
			box-shadow: inset var( --vfx-right-shadow-block-default );
		}

		fieldset.pills label:first-of-type{
			border-bottom-left-radius: var( --size-large-radius );
			border-top-left-radius: var( --size-large-radius );
		}

		fieldset.pills label:last-of-type{
			border-bottom-right-radius: var( --size-large-radius );
			border-top-right-radius: var( --size-large-radius );
		}

		/*****************************
		 * TAGS - TEXTAREA
		 *****************************/

		 textarea{
			background-color: var( --color-brightest );
			border: 1px solid var( --color-strut-input );
			border-radius: var( --size-radius );
			margin: 0;
			padding: 0.75rem 1rem;
			vertical-align: sub;
		 }

		/*****************************
		 * MOLECULES - FIELD
		 *****************************/

		.field{
			line-height: 1.2em;
		}

		.field label{
			display: inline-block;
		}

		.field label,
		.field input:not([type]),
		.field input[type="text"],
		.field input[type="password"],
		.field input[type="file"],
		.field select,
		.field textarea{
			width: 100%;
		}
	`;
}

export { css, unsafeCSS };
