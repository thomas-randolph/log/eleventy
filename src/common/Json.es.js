export function parse( string ){
	var json;

	try{
		json = JSON.parse( string );
	}
	catch( e ){
		json = string;
	}

	return json;
}
