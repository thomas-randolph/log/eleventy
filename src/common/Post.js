var { DateTime } = require( "luxon" );

function id( post ){
	return post.sys.id;
}

function isDraft( post ){
	return [ "hidden", "previewable" ].includes( post.fields.visibility );
}

function isFuture( post ){
	let now = ( new Date() ).toISOString();
	let authored = DateTime
		.fromISO( post.fields.authored )
		.toUTC()
		.toISO();

	return authored > now;
}

function isHidden( post ){
	return post.fields.visibility == "hidden";
}

function isRssExclusive( post ){
	return post.fields.rssExclusive || false;
}

function hiddenAtPromotion( promotion, post ){
	var levels = {
		"production": () => isDraft( post ) || isFuture( post ),
		"preview": () => isHidden( post ),
		"local": () => false
	};
	var hidden = true;

	if( levels[ promotion ] ){
		hidden = levels[ promotion ]();
	}

	return hidden;
}

function sortReverseChronologically( posts ){
	return posts.sort( ( alpha, beta ) => {
		let sort = 0;

		if( alpha.fields.authored > beta.fields.authored ){
			sort = -1;
		}

		if( alpha.fields.authored < beta.fields.authored ){
			sort = 1;
		}

		return sort;
	} );
}

module.exports = {
	id,
	isDraft,
	isFuture,
	isHidden,
	isRssExclusive,
	hiddenAtPromotion,
	sortReverseChronologically
}
