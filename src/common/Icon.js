var { library, icon, parse } = require( "@fortawesome/fontawesome-svg-core" );
var {
	"faSpinnerThird": fadSpinnerThird,
} = require( "./icon/subset/duotone.js" );
var {
	"faCalendar": falCalendar,
	"faCheck": falCheck,
	"faCloudUpload": falCloudUpload,
	"faEnvelope": falEnvelope,
	"faFileImage": falFileImage,
	"faGlobe": falGlobe,
	"faImage": falImage,
	"faLink": falLink,
	"faLock": falLock,
	"faLockOpen": falLockOpen,
	"faPaste": falPaste,
	"faPencilAlt": falPencilAlt,
	"faRss": falRss,
	"faTimes": falTimes,
	"faTrash": falTrash
} = require( "./icon/subset/light.js" );
var { emit } = require( "./Error.js" );

var icons = [
	fadSpinnerThird,
	falCalendar,
	falCheck,
	falCloudUpload,
	falEnvelope,
	falFileImage,
	falGlobe,
	falImage,
	falLink,
	falLock,
	falLockOpen,
	falPaste,
	falPencilAlt,
	falRss,
	falTimes,
	falTrash
];

function getLibrary(){
	library.add( ...icons );

	return library;
}

function getIconClasses( {
	spin,
	fixedWidth
} ){
	var classes = [];

	if( spin ){
		classes.push( "fa-spin" );
	}

	if( fixedWidth ){
		classes.push( "fa-fw" );
	}

	return classes;
}

function computeSVG( {
	"icon": iconName,
	type,
	transforms,
	title,
	classes
} ){
	var packs = {
		"duotone": "fad",
		"brands": "fab",
		"light": "fal",
		"regular": "far",
		"solid": "fas"
	};
	var svg = {
		"prefix": packs[ type ],
		iconName
	};
	var authoritative = icon( svg, {
		"transform": parse.transform( transforms ),
		title,
		classes
	} );

	if( !authoritative ){
		emit( `Unable to find icon definition for ${iconName} in the ${type} pack. Did you forget to subset it?` );
	}

	return authoritative ? authoritative.html[ 0 ] : "";
}

function getIcon( config ){
	getLibrary();

	return computeSVG( {
		"icon": config.icon,
		"type": config.type || "light",
		"transforms": config.transforms || "",
		"title": config.title || "",
		"classes": getIconClasses( {
			"spin": config.spin || false,
			"fixedWidth": config.fixedWidth || false
		} )
	} )
}

module.exports = {
	icon,
	icons,
	parse,
	getLibrary,
	getIconClasses,
	computeSVG,
	getIcon
};
