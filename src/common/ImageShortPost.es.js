import { posts } from "../data/imageShortPosts.js";
import { hiddenAtPromotion } from "./ShortPost.js";
export {
	htmlFromPost,
	hyperlinkedText,
	permalink,
	safeBody,
	title,
	getSubtype,
	isDraft
} from "./ShortPost.js";

function createDynamicCloudinaryUrl( baseSource, transformations ){
	var inject = baseSource.indexOf( "image/upload/v" ) + 13;

	return `${baseSource.substring( 0, inject )}${transformations}/${baseSource.substring( inject )}`;
}

export function getForPromotion( promotion ){
	return posts.filter( ( p ) => !hiddenAtPromotion( promotion, p ) );
}

export function dynamicImage( image ){
	var source = image.fields.source;
	var bare = source.substring( 0, source.lastIndexOf( "." ) );
	var ext = source.substring( source.lastIndexOf( "." ) + 1 );
	var multiPreview = createDynamicCloudinaryUrl( bare, "c_fit,f_auto,q_50,w_200,h_200" );
	var thumbnail = createDynamicCloudinaryUrl( bare, "c_fill_pad,f_auto,g_auto,q_50,w_100,h_100" );

	return {
		...image,
		"fields": {
			...image.fields,
			ext,
			multiPreview,
			thumbnail
		}
	};
}
