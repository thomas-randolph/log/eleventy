import { posts } from "../data/shortPosts.js";
import { id, isDraft, hiddenAtPromotion } from "./Post.js";

const SHORT_LIMIT = 42;

export { id, isDraft, hiddenAtPromotion };

export function htmlFromPost( body ){
	return newlinesAsBreaks( hyperlinkedText( body ) );
}

export function newlinesAsBreaks( content ){
	return content.replace( /\n/mg, "<br />" );
}

export function hyperlinkedText( content = "" ){
	return content.replace(
		/([^\S]|^)(((https?:\/\/)|(www\.))(\S+))/gi,
		function replacer( match, space, url ){
			var hyperlink = url;

			if( !hyperlink.match( "^https?://" ) ){
				hyperlink = "http://" + hyperlink;
			}

			return space + '<a href="' + hyperlink + '">' + url + "</a>";
		}
	);
}

export function permalink( post ){
	return `/post/${post.sys.id}.html`;
}

export function safeBody( post ){
	var reply = post.fields.replyingTo ? "↪ " : "";

	return `${reply}${post.fields.body.replace( /"/g, "&quot;" )}`;
}

export function title( post ){
	var safe = safeBody( post );
	var body = safe.substring( 0, SHORT_LIMIT - 1 );
	var tail = safe.length > SHORT_LIMIT ? "..." : "";

	return `${post.fields.author.fields.display}: &quot;${body}${tail}&quot;`;
}

export function getForPromotion( promotion ){
	return posts.filter( ( p ) => !hiddenAtPromotion( promotion, p ) );
}

export function getSubtype( post ){
	var type = "";

	if( post.fields.images && post.fields.images.length ){
		type = "image";
	}

	return type;
}
