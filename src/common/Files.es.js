export function bufferToBase64( buffer ){
	var bytes = new Uint8Array( buffer );
	var length = bytes.byteLength;
	var binary = "";

	for( let i = 0; i < length; i++ ){
		binary += String.fromCharCode( bytes[ i ] );
	}

	return btoa( binary );
}

export function readFileToBuffer( file ){
	var reader = new FileReader();

	return new Promise( ( resolve, reject ) => {
		reader.readAsArrayBuffer( file );

		reader.onload = function readerFileLoaded( fileLoadEvent ){
			resolve( fileLoadEvent.target.result );
		};

		reader.onerror = function readerFileError( fileErrorEvent ){
			reader.abort();
		};

		reader.onabort = function readerReadAborted(){
			reject( reader.error );
		};
	} );
}
