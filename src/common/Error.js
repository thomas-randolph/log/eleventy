var { dump } = require( "./Console.js" );

function emit( msg, trace, other ){
	dump( "error", msg, trace, other );
}

module.exports = { emit };
