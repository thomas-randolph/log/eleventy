var { md5 } = require( "./Crypto.js" );

function getGravatar( emailAddress ){
	var cleanedEmail = emailAddress.trim().toLowerCase();

	return `https://www.gravatar.com/avatar/${md5( cleanedEmail )}?d=identicon&s=200`;
}

function hasWebsite( author ){
	var contacts = author.fields.contacts;

	return contacts ? contacts.some( ( contact ) => contact.fields.type == "website" ) : false;
}

function website( author ){
	var site = "";

	if( hasWebsite( author ) ){
		site = author.fields.contacts
			.filter( ( contact ) => contact.fields.type == "website" )[ 0 ]
			.fields.value;
	}

	return site;
}

function hasEmail( author ){
	var contacts = author.fields.contacts;

	return contacts ? contacts.some( ( contact ) => contact.fields.type == "email" ) : false;
}

function twitter( author ){
	var twitterUsername = "";
	var contacts = author.fields.contacts || [];
	var twitters = contacts.filter( ( contact ) => contact.fields.type == "twitter" );
	var username = twitters[ 0 ];

	if( username ){
		twitterUsername = username.fields.value;
	}

	return twitterUsername;
}

function getBestEmailAddress( author ){
	var def = { "fields": { "contacts": [], "primaryContact": { "fields": {} } } };
	var auth = Object.assign( def, author );
	var contacts = auth.fields.contacts;
	var primary = auth.fields.primaryContact.fields;
	var emailAddress = "";

	if( primary.type == "email" ){
		emailAddress = primary.value;
	}
	else{
		contacts.some( ( contact ) => {
			if( contact.fields.type == "email" ){
				emailAddress = contact.fields.value;

				return true;
			}
		} );
	}

	return emailAddress;
}

function name( author ){
	return author.fields.display;
}

function getProfilePictureUrl( author ){
	var def = { "fields": { "image": "" } };
	var url = Object.assign( def, author ).fields.image;

	if( !url ){
		url = getGravatar( getBestEmailAddress( author ) );
	}

	return url;
}

module.exports = {
	hasWebsite,
	website,
	hasEmail,
	twitter,
	getBestEmailAddress,
	name,
	getProfilePictureUrl
}
