var { md5 } = require( "./md5.js" );

function hex( buffer ){
	var hexCodes = [];
	var view = new DataView( buffer );

	for( let i = 0; i < view.byteLength; i += 4 ){
		hexCodes.push( `00000000${view.getUint32( i ).toString( 16 )}`.slice( -8 ) );
	}

	return hexCodes.join( "" );
}

function makeKey( secret, algo = "SHA-512", usages = [ "sign" ] ){
	var buffer = new TextEncoder( "utf-8" ).encode( secret );

	return crypto
		.subtle
		.importKey(
			"raw",
			buffer,
			{
				"name": "HMAC",
				"hash": { "name": algo }
			},
			false,
			usages
		);
}

function hash( str, algo = "SHA-512" ){
	var buffer = new TextEncoder( "utf-8" ).encode( str );

	return crypto
		.subtle
		.digest( algo, buffer )
		.then( hex );
}

async function hmac( secret, message, algo = "SHA-512" ){
	var buffer = new TextEncoder( "utf-8" ).encode( message );
	var key = await makeKey( secret, algo );

	return crypto
		.subtle
		.sign(
			"HMAC",
			key,
			buffer
		)
		.then( hex );
}


 module.exports = {
	hash,
	hmac,
	md5
};
