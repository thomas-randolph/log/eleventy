var {
	"permalink": longPermalink,
	"slug": longSlug
} = require( "../common/LongPost.js" );
var {
	hiddenAtPromotion,
	isRssExclusive,
	sortReverseChronologically
} = require( "../common/Post.js" );
var { getIcon } = require( "../common/Icon.js" );

class Index{
	data( ...args ){
		return {
			"permalink": "/index.html"
		};
	}
	render( config ){
		var long = sortReverseChronologically(
			config.longPosts.filter( ( post ) => !hiddenAtPromotion( "production", post ) && !isRssExclusive( post ) )
		);
		var longSummaries = "";

		long.forEach( ( post ) => {
			longSummaries += `
			<long-post-summary>
				<div class="post long summary">
					<h1 id="${ longSlug( post ) }">
						<a href="${ longPermalink( post ) }.html">
							${ post.fields.title }
						</a>
					</h1>
				</div>
			</long-post-summary>
			`;
		} );

		return `
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta name="monetization" content="$ilp.uphold.com/gF8aFrbb6P3N">

		<title>All Content | console.blog | Thomas O. Randolph</title>

		<link rel="stylesheet" href="/css/screen.min.css" media="screen" />
		<link rel="alternate" type="application/rss+xml" title="RSS" href="/rss.xml" />
	</head>
	<body>
		<header>
			<a href="/">
				<code>console.blog(</code>
			</a>
			<span>
				<a type="application/rss+xml" title="RSS" href="/rss.xml" rel="alternate">
					<x-icon><span class="icon">${getIcon( { "icon": "rss" } )}</span></x-icon>
				</a>
				,
			</span>
		</header>
		<div class="view index">
			<div class="head">
				<img alt="Portrait photo of Thomas Randolph in a casual setting." src="https://www.gravatar.com/avatar/8361ba9354d54301d67cf2d0ded0f504?s=100">
				<h1>Thomas O. Randolph</h1>
				<a href="/bookshelf">Bookshelf</a>
			</div>
			<div id="cf-worker-long" class="long">
				${ longSummaries }
			</div>
			<div id="cf-worker-short" class="short"></div>
		</div>
		<footer>
			<span>
				,
				<a href="https://www.contentful.com/" rel="nofollow" target=“_blank”>
					<img src="https://images.contentful.com/fo9twyrwpveg/44baP9Gtm8qE2Umm8CQwQk/c43325463d1cb5db2ef97fca0788ea55/PoweredByContentful_LightBackground.svg" style="max-width:100px;width:100%;" alt="Powered by Contentful" />
				</a>
			</span>
			<code>);</code>
		</footer>
	</body>
</html>
		`;
	}
}

module.exports = Index;
