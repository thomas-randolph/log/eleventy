var { DateTime } = require( "luxon" );

var { getBestEmailAddress, getProfilePictureUrl, website, twitter } = require( "./src/common/Author.js" );
var { getIcon } = require( "./src/common/Icon.js" );

module.exports = function( config ){
	config.addFilter( "isDraft", function( post ){
		return [ "hidden", "previewable" ].includes( post.fields.visibility );
	} );

	config.addFilter( "icon", function( iconConfig ){
		return `<x-icon><span class="icon">${getIcon( iconConfig )}</span></x-icon>`;
	} );

	config.addFilter( "shortDate", function( iso ){
		return DateTime.fromISO( iso )
			.setZone( "America/Denver" )
			.toFormat( "yyyy-MM-dd HH:mm" );
	});

	config.addFilter( "authorPicture", function( author ){
		return getProfilePictureUrl( author );
	} );
	config.addFilter( "authorEmail", function( author ){
		return getBestEmailAddress( author );
	} );
	config.addFilter( "authorWebsite", function( author ){
		return website( author );
	} );
	config.addFilter( "authorTwitter", function( author ){
		return twitter( author );
	} );

	return {
		"dir": {
			"input": "src/views",
			"output": "public/",
			"data": "../../generate/"
		}
	}
}
